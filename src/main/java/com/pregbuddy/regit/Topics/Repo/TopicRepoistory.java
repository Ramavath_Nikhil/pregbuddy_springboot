package com.pregbuddy.regit.Topics.Repo;

import com.pregbuddy.regit.Topics.Models.Topic;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by nikhilramavath on 20/03/18.
 */
@Repository
public interface TopicRepoistory extends MongoRepository<Topic, String> {



}
