package com.pregbuddy.regit.Topics.Models;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.CreatedDate;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by nikhilramavath on 20/03/18.
 */
public class Actions {

    @org.springframework.data.annotation.Id
    private ObjectId Id;

    @NotNull(message = "user id is mandatory")
    private String userId;


    @NotNull(message = "topic id is mandatory")
    private String topicId;


    @NotNull(message = "action is mandatory")
    private int action;

    @CreatedDate
    private Date created;



    public ObjectId getId() {
        return Id;
    }

    public void setId(ObjectId id) {
        Id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }


}
