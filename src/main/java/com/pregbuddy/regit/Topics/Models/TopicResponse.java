package com.pregbuddy.regit.Topics.Models;

import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by nikhilramavath on 20/03/18.
 */
public class TopicResponse {

    private List<TopicWithUpVoteAndDownVote> topic;
    private Pageable pageable;


    public List<TopicWithUpVoteAndDownVote> getTopic() {
        return topic;
    }

    public void setTopic(List<TopicWithUpVoteAndDownVote> topic) {
        this.topic = topic;
    }

    public Pageable getPageable() {
        return pageable;
    }

    public void setPageable(Pageable pageable) {
        this.pageable = pageable;
    }


}
