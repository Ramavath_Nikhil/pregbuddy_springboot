package com.pregbuddy.regit.Topics.Models;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.DBRef;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

/**
 * Created by nikhilramavath on 20/03/18.
 */
public class Topic {

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    @org.springframework.data.annotation.Id
    private String Id;

    @NotNull(message = "topic should not be empty")
    @Size(min = 2, max = 255, message = "topic should have atleast 2 characters and maxmium 255 character")
    private String topicText;
    @NotNull(message = "user id is mandatory")
    private String userId;

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    @CreatedDate
    private Date created;

    @LastModifiedDate
    private Date modified;

    private int upVoteCount;

    public int getUpVoteCount() {
        return upVoteCount;
    }

    public void setUpVoteCount(int upVoteCount) {
        this.upVoteCount = upVoteCount;
    }

    public int getDownVoteCount() {
        return downVoteCount;
    }

    public void setDownVoteCount(int downVoteCount) {
        this.downVoteCount = downVoteCount;
    }

    private int downVoteCount;





    public String getTopicText() {
        return topicText;
    }

    public void setTopicText(String topicText) {
        this.topicText = topicText;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
