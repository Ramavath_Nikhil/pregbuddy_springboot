package com.pregbuddy.regit.Topics.Models;

/**
 * Created by nikhilramavath on 20/03/18.
 */
public class TopicWithUpVoteAndDownVote {

    private Topic topic;
    private boolean upVoted, downVoted;

    public TopicWithUpVoteAndDownVote(boolean upVoted, boolean downVoted, Topic topic) {
        this.upVoted = upVoted;
        this.downVoted = downVoted;
        this.topic = topic;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public boolean isUpVoted() {
        return upVoted;
    }

    public void setUpVoted(boolean upVoted) {
        this.upVoted = upVoted;
    }

    public boolean isDownVoted() {
        return downVoted;
    }

    public void setDownVoted(boolean downVoted) {
        this.downVoted = downVoted;
    }
}
