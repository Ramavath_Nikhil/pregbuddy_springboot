package com.pregbuddy.regit.Topics.Models;

/**
 * Created by nikhilramavath on 21/03/18.
 */
public class UpVoteActions extends Actions {


    public UpVoteActions(String topicId) {
        super();
        this.setTopicId(topicId);
    }

    @Override
    public boolean equals(Object obj) {
        if (this.getTopicId().equals(((UpVoteActions) obj).getTopicId()))
            return true;
        else
            return false;
    }
}

