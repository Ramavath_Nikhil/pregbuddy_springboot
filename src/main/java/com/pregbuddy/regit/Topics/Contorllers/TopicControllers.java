package com.pregbuddy.regit.Topics.Contorllers;

import com.pregbuddy.regit.Topics.Models.*;
import com.pregbuddy.regit.Topics.Repo.TopicRepoistory;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperationContext;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by nikhilramavath on 20/03/18.
 */

@RestController
@EnableMongoAuditing
public class TopicControllers {

    @Autowired
    TopicRepoistory topicRepoistory;


    @Autowired
    MongoOperations mongoOperation;

    @RequestMapping(value = "/gettopics//{userID}", method = RequestMethod.GET)
    public TopicResponse getAllTopics(@PathVariable String userID, Pageable pageable) {


//        String userId = "asdas";
//        Sort sortByCreatedAtDesc = new Sort(Sort.Direction.DESC, "createdAt");
//        Page<Topic> topics = topicRepoistory.findAll(pageable);
//
//
//        LookupOperation lookupOperation = LookupOperation.newLookup().
//                from("actions").
//                localField("topicId").
//                foreignField("id").
//                as("posts");
//
//
//        Aggregation aggregation = Aggregation.newAggregation(lookupOperation);
//        List<TopicResponse> results = mongoOperation.aggregate(aggregation, "topic", TopicResponse.class).getMappedResults();

        Page<Topic> topics = topicRepoistory.findAll(pageable);


        AggregationOperation match = Aggregation.match(Criteria.where("action").is(1));
        AggregationOperation userIdAggreation = Aggregation.match(Criteria.where("userId").is(userID));
        Aggregation aggregation = Aggregation.newAggregation(match, userIdAggreation);
        List<UpVoteActions> upVotes = mongoOperation.aggregate(aggregation, "actions", UpVoteActions.class).getMappedResults();


        match = Aggregation.match(Criteria.where("action").is(-1));
        userIdAggreation = Aggregation.match(Criteria.where("userId").is(userID));
        aggregation = Aggregation.newAggregation(match, userIdAggreation);
        List<UpVoteActions> downVotes = mongoOperation.aggregate(aggregation, "actions", UpVoteActions.class).getMappedResults();
        List<TopicWithUpVoteAndDownVote> topicWithWithUpVoteAndDownVote = new ArrayList<>();


        for (Topic topic : topics.getContent()
                ) {

            if (upVotes.contains(new UpVoteActions(topic.getId())) && downVotes.contains(new UpVoteActions(topic.getId()))) {

                System.out.print("enter both");
                topicWithWithUpVoteAndDownVote.add(new TopicWithUpVoteAndDownVote(true, true, topic));
            } else if (upVotes.contains(new UpVoteActions(topic.getId()))) {
                System.out.print("enter up");
                topicWithWithUpVoteAndDownVote.add(new TopicWithUpVoteAndDownVote(true, false, topic));
            } else if (downVotes.contains(new UpVoteActions(topic.getId()))) {
                System.out.print("enter down");
                topicWithWithUpVoteAndDownVote.add(new TopicWithUpVoteAndDownVote(false, true, topic));
            } else {
                System.out.print("enter none");
                topicWithWithUpVoteAndDownVote.add(new TopicWithUpVoteAndDownVote(false, false, topic));
            }

        }

        TopicResponse topicRespone = new TopicResponse();
        topicRespone.setPageable(pageable);
        topicRespone.setTopic(topicWithWithUpVoteAndDownVote);

        return topicRespone;
    }


    @RequestMapping(value = "/createtopic", method = RequestMethod.POST)
    public Topic createUser(@Valid @RequestBody Topic topic) {

        return topicRepoistory.save(topic);
    }


    public class CustomAggregationOperation implements AggregationOperation {
        private Document operation;

        public CustomAggregationOperation(Document operation) {
            this.operation = operation;
        }


        @Override
        public Document toDocument(AggregationOperationContext aggregationOperationContext) {
            return aggregationOperationContext.getMappedObject(operation);
        }
    }


}
