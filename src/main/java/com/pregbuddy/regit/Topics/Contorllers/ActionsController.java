package com.pregbuddy.regit.Topics.Contorllers;

import com.pregbuddy.regit.Topics.Models.Actions;
import com.pregbuddy.regit.Topics.Models.Topic;
import com.pregbuddy.regit.Topics.Repo.ActionRepoistory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;


/**
 * Created by nikhilramavath on 20/03/18.
 */

@RestController
@EnableMongoAuditing
public class ActionsController {


    @Autowired
    ActionRepoistory actionRepoistory;

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    MongoOperations mongoOperations;

    @RequestMapping(value = "/postaction", method = RequestMethod.POST)
    public Actions postAction(@Valid @RequestBody Actions actions) {

        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(actions.getTopicId()));

        Topic topic = mongoOperations.findOne(query, Topic.class);

        if (actions.getAction() == 1)
            topic.setUpVoteCount(topic.getUpVoteCount() + 1);
        else
            topic.setDownVoteCount(topic.getDownVoteCount() + 1);
        mongoOperations.save(topic);

        return actionRepoistory.save(actions);
    }

    @RequestMapping(value = "/getaction", method = RequestMethod.GET)
    public List getUpVotes() {
        GroupOperation groupByStateAndSumPop = group("topicId")
                .sum("action").as("statePop");
        MatchOperation matchStage = Aggregation.match(new Criteria("action").is(1));

        Aggregation aggregation = newAggregation(
                matchStage);

        AggregationResults<Actions> result = mongoTemplate.aggregate(
                aggregation, "actions", Actions.class);

        return result.getMappedResults();
    }
}
