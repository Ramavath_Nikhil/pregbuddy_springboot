package com.pregbuddy.regit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.EnableMongoAuditing;

@SpringBootApplication

public class RegitApplication {

    public static void main(String[] args) {
        SpringApplication.run(RegitApplication.class, args);
    }
}
